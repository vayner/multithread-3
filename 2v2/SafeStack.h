#pragma once

#include <stack>
#include <mutex>
#include <stdlib.h>

template<typename T>
class SafeStack {
public:

    SafeStack(){
    };

    ~SafeStack() {
    };

    T& top() {
        std::lock_guard<std::mutex> guard(lock);
        return data.top();
    };

    bool empty() {
        std::lock_guard<std::mutex> guard(lock);
        return data.empty();
    }

    size_t size() {
        std::lock_guard<std::mutex> guard(lock);
        return data.size();
    }

    void push(const T& value) {
        std::lock_guard<std::mutex> guard(lock);
        data.push(value);
    }

    void push(T&& value) {
        std::lock_guard<std::mutex> guard(lock);
        data.push(value);
    }

    void pop() {
        std::lock_guard<std::mutex> guard(lock);
        data.pop();
    }

private:

    std::stack<T> data;
    std::mutex lock;
};