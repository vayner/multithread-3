#include <stdio.h>
#include <thread>

#include <string>
#include <array>

#include "SafeStack.h"

const int NUM_THREADS = 100;
const int ITERATIONS = 1000;

SafeStack<std::string> stackA;
SafeStack<std::string*> stackB;


void pushThread() {

    for (int i = 0; i < ITERATIONS; ++i) {
        stackA.push("HAHA");
        stackA.pop();
        stackA.push("LALALA");
    }
}

void popThread() {

    for (int i = 0; i < ITERATIONS; ++i) {
        std::string* top = stackB.top();
        stackB.pop();
        printf("pop: %s\n", top->c_str());
    }
}

int main () {
    std::array<std::thread, NUM_THREADS> threads;

    for (std::thread &t : threads) {
        t = std::thread(pushThread);
    }

    for (std::thread &t : threads) {
        t.join();
    }

    printf("%lu == %i  %s\n", stackA.size(), ITERATIONS * NUM_THREADS, stackA.top().c_str());

    for (int i = 0; i < ITERATIONS * NUM_THREADS; ++i) {
        stackB.push(new std::string(std::to_string(i)));
    }

    for (std::thread &t : threads) {
        t = std::thread(popThread);
    }

    for (std::thread &t : threads) {
        t.join();
    }

    return 0;
}