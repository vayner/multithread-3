Bugs and stuff
* Scope of values and template stack, ended up using pointers.
* Where to have a template class's definitions. Definitions ended up in .h, could be renamed to .hpp to reflect it.
* Allocation and semaphores. Ended up with malloc() and free() with sem_t pointers