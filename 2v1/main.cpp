#include <stdio.h>
#include <pthread.h>

#include <string>

#include "SafeStack.h"

const int NUM_THREADS = 100;
const int ITERATIONS = 1000;

SafeStack<std::string> stackA;
SafeStack<std::string*> stackB;


void* pushThread(void*) {

    for (int i = 0; i < ITERATIONS; ++i) {
        stackA.push("HAHA");
        stackA.pop();
        stackA.push("LALALA");
    }

    return 0;
}

void* popThread(void*) {

    for (int i = 0; i < ITERATIONS; ++i) {
        std::string* top = stackB.top();
        stackB.pop();
        printf("pop: %s\n", top->c_str());
    }


    return 0;
}

int main () {
    pthread_t threads[NUM_THREADS];

    for (int i = 0; i < NUM_THREADS; ++i) {
        pthread_create(&threads[i], 0, pushThread, NULL);
    }

    for (int i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], 0);
    }

    printf("%lu == %i  %s\n", stackA.size(), ITERATIONS * NUM_THREADS, stackA.top().c_str());

    for (int i = 0; i < ITERATIONS * NUM_THREADS; ++i) {
        stackB.push(new std::string(std::to_string(i)));
    }

    for (int i = 0; i < NUM_THREADS; ++i) {
        pthread_create(&threads[i], 0, popThread, NULL);
    }

    for (int i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], 0);
    }

    return 0;
}