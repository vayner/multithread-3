#pragma once

#include <stack>
#include <semaphore.h>
#include <stdlib.h>

template<typename T>
class SafeStack {
public:

    SafeStack(){
        lock = (sem_t *) malloc(sizeof(sem_t));
        sem_init(lock, 0, 1);
    };

    ~SafeStack() {
        sem_destroy(lock);
        free(lock);
    };

    T& top() {
        sem_wait(lock);
        auto& r = data.top();
        sem_post(lock);

        return r;
    };

    const T& top() const {
        sem_wait(lock);
        const auto& r = data.top();
        sem_post(lock);

        return r;
    };

    bool empty() const{
        sem_wait(lock);
        bool r = data.empty();
        sem_post(lock);

        return r;
    }

    size_t size() const {
        sem_wait(lock);
        size_t r = data.size();
        sem_post(lock);

        return r;
    }

    void push(const T& value) {
        sem_wait(lock);
        data.push(value);
        sem_post(lock);
    }

    void push(T&& value) {
        sem_wait(lock);
        data.push(value);
        sem_post(lock);
    }

    void pop() {
        sem_wait(lock);
        data.pop();
        sem_post(lock);
    }

private:

    std::stack<T> data;
    sem_t* lock;
};