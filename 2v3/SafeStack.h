#pragma once

#include <stack>
#include <mutex>
#include <stdlib.h>

template<typename T>
class SafeStack : public std::stack<T>, public std::mutex {
};