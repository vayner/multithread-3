#include <semaphore.h>

#pragma once

class Singleton {
public:

    static Singleton* getInstance();

private:

    Singleton();
    ~Singleton();

    static Singleton* self;
    static sem_t lock;

    static int staticy;

    static int staticFunc();
};