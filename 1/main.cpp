#include "Singleton.h"
#include "pthread.h"
#include "stdio.h"

const int NUM_THREADS = 4;

void* conflictThread(void*) {
    Singleton* ref = Singleton::getInstance();
    printf("%x\n", ref);

    return 0;
}

int main() {

    pthread_t threads[NUM_THREADS];

    for (int i = 0; i < NUM_THREADS; ++i) {
        pthread_create(&threads[i], 0, conflictThread, NULL);
    }


    for (int i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], 0);
    }

    return 0;
}