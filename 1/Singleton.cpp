#include "Singleton.h"

Singleton* Singleton::self = nullptr;
int Singleton::staticy = Singleton::staticFunc();
sem_t Singleton::lock;


int Singleton::staticFunc() {
    sem_init(&lock, 0, 1);
    return 0;
}

Singleton* Singleton::getInstance() {
    sem_wait(&lock);

    if(self == nullptr) {
        self = new Singleton();
    }

    sem_post(&lock);
    return self;
}

Singleton::Singleton() {
    staticy++;
}

Singleton::~Singleton() {
    sem_destroy(&lock);
}